# How to recreate the project

In this Readme you will be guided through steps needed in order to recreate our DevOps course project.

#### Prerequisites
Our approach was requires you to have the following:
- Unix system
- Installed Git 
- Installed docker
- Google Account
- GitLab account
- You should have a banking card (no money will be withdrawn)

## Local environment
In this step we will explain how to set up your local environment for our project.

### Clone the project 
The first thing you should do is to clone our GitLab repository. You can do so by running
the following commands:

```
git clone https://gitlab.com/italiandevopsgroup/petclinic.git
cd spring-petclinic
./mvnw  -Dmaven.test.skip=true package
```
This will download the project on your machine as well as build the project with all required dependencies.

### Run the project locally

In order to run the project locally, simply run
```bash
docker-compose up
```

Once the output in the console reports that the project is up and running, you can access it though a web browser
at `localhost:8080`.

This is it for now for the local environment, we will get back to it later to set up a remote repository and add 
pre-push hooks.

##GitLab set-up
In this section we will describe how to set up the GitLab part of the project. 

### Create a new group
Go to `https://gitlab.com/` and sign in (or create an account if you do npt have it yet). Once you are logged in, go to
the `Groups` tab, then to `Your groups` and click on `New group`. Assign a name to the group, in our case it is 
`ItalianDevOpsGroup`.

### Create a new project
Once you have a group, go to `Group overview` and click on the `New project` button. Create a 
new project, in our case it is `PetClinic`.

### Add Kubernetes cluster
There is an important prerequisite for this step: you will need to create a 
[Google Cloud Account](https://cloud.google.com). This will require you to provide a payment method. However, Google
provides 300$ deposit and will not charge you anything afterwards unless you will confirm that after 300$ are spent.
Now create a project on Google Cloud. In our case it is `italianpetclinic` and the location is `us-central1-a`.
Finally, you need to enable the APIs we will use. For this do the following:
1. Go to Google Cloud Platform 
2. go to the burger menu
3. click on `Api & Services`
4. click `+ Enable Apis and services`
5. in the search filed find `Compute Engine API`
6. click `Enable`

Wait for the API to be enabled. Now you need to do the same for Kubernetes. For this repeat steps 1 - 4 and then:
5. in the search filed find `Kurnetes Engine API`
6. in the search filed find `Compute Engine API`


Once you have done the above steps and made sure that all changes have propagated through google cloud, 
go to the group you created on `GitLab`, then 
to the `Kubernetes` tab and click `Add Kubernetes cluster`. Then make sure that the `Create new Cluster on GKE` 
tab is open and click on `Sign in with Google`. Go through the authentication process with Google and then fill out the 
`Enter the details for your Kubernetes cluster` form, our set-up was:
* name: petclinic-cluster
* Google Cloud Platform project: italianpetclinic
* Machine type: g1-small
* GitLab-managed cluster - checked

In case there has been an error, check that you enabled all APIs, and delete the integration (for this
go to the cluster -> Advancrd setting -> Expand -> Remove integration)

Once the cluster is added, click on its name and observe the `Applications` section. In this section, install 
`GitLab Runner` and `Helm Tiller`. Make sure to wait for installation to finish.

## Google Cloud configuration

### Access roles
In this sections we will deal with access rights and roles on Google Cloud. The reason is because we will need to 
script the deployment process by accessing the Kubernetes cluster though special google cloud cli and for this 
we will need to have special service account, in order to have an authorized access to our google cloud VMs. 

Steps:
1. Go to `Burger menu` -\> `IAM Admin`
2. Find service account (by the name column) 
3. Click on pencil
4. Set the following roles
        
    1. Compute Admin
    2. Kubernetes Engine Cluster Admin
    3. Cloud Datastore Owner
    4. Owner
    
   Please preserve the order and the set of roles, otherwise there will be problems with permissions!
5. Point on `Api & Services` -\> `Credentials`
6. `Create credential` -\> `Service account key`
7. `Default account`
8. `JSON` -\> create. A file will be downloaded to your computer
9. Open the file and copy the content 
10. On GitLab:
    1. go to `Projects`
    2. go to "you-project-name" 
    3. `Settings` (left side bar) 
    5. `CI/CD`
    6. Click `Expand` at `Variables`
    7. Set a variable: key = GOOGLE_KEY, value = (copy from the credentials file)
    8. Save variable


## Change Pipeline configuration

Now you need to change a few configuration files to make them specific for your versions of the set-up. 
You can find them in the root directory of the project.

In `deployment.yaml` 
- Rewrite this line `image: registry.gitlab.com/<<your group>>/<<project name>>`
- And this line `name: registry.gitlab.com/<<your group>>/<<project name>>`

Now you need to create a Deploy Token as described [here](https://docs.gitlab.com/ee/user/project/deploy_tokens/).
Once you have the login and the password for your token:

In `.gitlab-ci.yml`
- In `docker-build` stage
    1. Replace all occurrences to `registry.gitlab.com/<<your group>>/<<project name>>c` 
- In `k8s-deploy-staging` and `k8s-deploy-production`
    1. replace `gcloud config set project <<your Google Cloud prokect>>`
    2. replace `gcloud container clusters get-credentials <<your Kubernetes Cluster name>>`
    3. replace `  - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username='<<your deploy tocken username>>' --docker-password='<<your deploy tocken password>>' --docker-email=<<your email>>`


## Create namespaces
Now you need to create different namespaces for different deployments.

On Google Cloud do the following:
1. In the burger menu go to `Kubernetes Engine` -\> `Clusters`
2. Find the cluster you created and click `Connect` -\> `Google Shell`
3. Wait for it to load
4. Press Enter to run the generated command (will be there automatically)
5. Run the following:


```bash
touch namespace-dev.json
echo '{                             
  "apiVersion": "v1",
  "kind": "Namespace",
  "metadata": {
    "name": "development",
    "labels": {
      "name": "development"
    }
  }
}' > namespace-dev.json

kubectl create -f namespace-prod.json

```

```bash
touch namespace-prod.json
echo '{                              
  "apiVersion": "v1",
  "kind": "Namespace",
  "metadata": {
    "name": "production",
    "labels": {
      "name": "production"
    }
  }
}' > namespace-prod.json
kubectl create -f namespace-prod.json
```

This will create two namespaces.

### Setting VCS policies
Before doing anything with the project, make sure that your repository has 2 essential branches ```develop``` and ```master```.
If it does not, create them. (Names are case-sensitive).

First of all, you should go to your repository, then open Settings -> Repository.
Expand the "Push Rules" section.

Check:
- Committer restriction
- Check whether author is a GitLab user

In Commit message field add:
 ```(Development|Fix|Implement|Delete|Patch|Update|Add|Merge)\s.{10, 300}$```
 
In Branch name field add: ```development|master|(feature|hotfix|fix)-[0-9]*-.*$```

Sometimes you might face problems with matching regexp because of encoding issues. So, in this case please type regexps manually.

In Prohibited file names add: ```(jar|exe)$```


In Maximum file size (MB) add: ```10``` 

Branch protection is optional and can be found below.

### Adding pre-push stage
PREREQUISISTE: Make sure that you can see hidden files in your file explorer.

Copy file with name ```pre-push``` from ```.githooks``` folder to ```.git/hooks```.

Open ```.git/hooks``` in shell and make it executable by running this command: ```chmod +x pre-commit```.


## Launching the pipeline
```
git remote add test <<Link to your remote git repository>>
git push test --no-verify
``` 
This should launch the pre-push stage and afterwards push the project to your repo. 

Now you can go to your project's GitLab web page and click on `CI/CD` in the side bar. If you followed this document
accurately, you will see visualisation your first deployment pipeline. It will have four stages and you will be able to 
see which one is currently running. IMPORTANT!!! Because of the weak hardware we have chosen, some of the stages may 
fail. In this case just click on the failed stage and click `Retry`. Same may happen with the deployment stage because
of gcloud issues with security. A `Retry` of the stage may help. 

## Final steps

Once you see all 4 stages have been passed successfully, you can go to your google cloud console again, to
`Kubernetes cluster`, then to `Workloads` and click on the project your created (probably petclinic). You will see
a message that goes "To let others access your deployment...". Click the `Expose` button there. In the following window
expose port 8080 and confirm. Now go to `Services & Ingress` and find your service. You can access the application by
the given link.

Done!




# Spring PetClinic Sample Application [![Build Status](https://travis-ci.org/spring-projects/spring-petclinic.png?branch=master)](https://travis-ci.org/spring-projects/spring-petclinic/)
Deploy this sample application to Pivotal Web Services:

<a href="https://push-to.cfapps.io?repo=https%3A%2F%2Fgithub.com%2Fspring-projects%2Fspring-petclinic.git">
    <img src="https://push-to.cfapps.io/ui/assets/images/Push-to-Pivotal-Light-with-Shadow.svg" width="180" alt="Push" align="center">
</a>

## Understanding the Spring Petclinic application with a few diagrams
<a href="https://speakerdeck.com/michaelisvy/spring-petclinic-sample-application">See the presentation here</a>

## Running petclinic locally
Petclinic is a [Spring Boot](https://spring.io/guides/gs/spring-boot) application built using [Maven](https://spring.io/guides/gs/maven/). You can build a jar file and run it from the command line:


```
git clone https://github.com/spring-projects/spring-petclinic.git
cd spring-petclinic
./mvnw package
java -jar target/*.jar
```

You can then access petclinic here: http://localhost:8080/

<img width="1042" alt="petclinic-screenshot" src="https://cloud.githubusercontent.com/assets/838318/19727082/2aee6d6c-9b8e-11e6-81fe-e889a5ddfded.png">

Or you can run it from Maven directly using the Spring Boot Maven plugin. If you do this it will pick up changes that you make in the project immediately (changes to Java source files require a compile as well - most people use an IDE for this):

```
./mvnw spring-boot:run
```

## In case you find a bug/suggested improvement for Spring Petclinic
Our issue tracker is available here: https://github.com/spring-projects/spring-petclinic/issues


## Database configuration

In its default configuration, Petclinic uses an in-memory database (HSQLDB) which
gets populated at startup with data. A similar setup is provided for MySql in case a persistent database configuration is needed.
Note that whenever the database type is changed, the app needs to be run with a different profile: `spring.profiles.active=mysql` for MySql.

You could start MySql locally with whatever installer works for your OS, or with docker:

```
docker run -e MYSQL_ROOT_PASSWORD=petclinic -e MYSQL_DATABASE=petclinic -p 3306:3306 mysql:5.7.8
```

Further documentation is provided [here](https://github.com/spring-projects/spring-petclinic/blob/master/src/main/resources/db/mysql/petclinic_db_setup_mysql.txt).

## Working with Petclinic in your IDE

### Prerequisites
The following items should be installed in your system:
* Java 8 or newer.
* git command line tool (https://help.github.com/articles/set-up-git)
* Your preferred IDE 
  * Eclipse with the m2e plugin. Note: when m2e is available, there is an m2 icon in `Help -> About` dialog. If m2e is
  not there, just follow the install process here: https://www.eclipse.org/m2e/
  * [Spring Tools Suite](https://spring.io/tools) (STS)
  * IntelliJ IDEA
  * [VS Code](https://code.visualstudio.com)

### Steps:

1) On the command line
```
git clone https://github.com/spring-projects/spring-petclinic.git
```
2) Inside Eclipse or STS
```
File -> Import -> Maven -> Existing Maven project
```

Then either build on the command line `./mvnw generate-resources` or using the Eclipse launcher (right click on project and `Run As -> Maven install`) to generate the css. Run the application main method by right clicking on it and choosing `Run As -> Java Application`.

3) Inside IntelliJ IDEA

In the main menu, choose `File -> Open` and select the Petclinic [pom.xml](pom.xml). Click on the `Open` button.

CSS files are generated from the Maven build. You can either build them on the command line `./mvnw generate-resources`
or right click on the `spring-petclinic` project then `Maven -> Generates sources and Update Folders`.

A run configuration named `PetClinicApplication` should have been created for you if you're using a recent Ultimate
version. Otherwise, run the application by right clicking on the `PetClinicApplication` main class and choosing
`Run 'PetClinicApplication'`.

4) Navigate to Petclinic

Visit [http://localhost:8080](http://localhost:8080) in your browser.


## Looking for something in particular?

|Spring Boot Configuration | Class or Java property files  |
|--------------------------|---|
|The Main Class | [PetClinicApplication](https://github.com/spring-projects/spring-petclinic/blob/master/src/main/java/org/springframework/samples/petclinic/PetClinicApplication.java) |
|Properties Files | [application.properties](https://github.com/spring-projects/spring-petclinic/blob/master/src/main/resources) |
|Caching | [CacheConfiguration](https://github.com/spring-projects/spring-petclinic/blob/master/src/main/java/org/springframework/samples/petclinic/system/CacheConfiguration.java) |

## Interesting Spring Petclinic branches and forks

The Spring Petclinic master branch in the main [spring-projects](https://github.com/spring-projects/spring-petclinic)
GitHub org is the "canonical" implementation, currently based on Spring Boot and Thymeleaf. There are
[quite a few forks](https://spring-petclinic.github.io/docs/forks.html) in a special GitHub org
[spring-petclinic](https://github.com/spring-petclinic). If you have a special interest in a different technology stack
that could be used to implement the Pet Clinic then please join the community there.


## Interaction with other open source projects

One of the best parts about working on the Spring Petclinic application is that we have the opportunity to work in direct contact with many Open Source projects. We found some bugs/suggested improvements on various topics such as Spring, Spring Data, Bean Validation and even Eclipse! In many cases, they've been fixed/implemented in just a few days.
Here is a list of them:

| Name | Issue |
|------|-------|
| Spring JDBC: simplify usage of NamedParameterJdbcTemplate | [SPR-10256](https://jira.springsource.org/browse/SPR-10256) and [SPR-10257](https://jira.springsource.org/browse/SPR-10257) |
| Bean Validation / Hibernate Validator: simplify Maven dependencies and backward compatibility |[HV-790](https://hibernate.atlassian.net/browse/HV-790) and [HV-792](https://hibernate.atlassian.net/browse/HV-792) |
| Spring Data: provide more flexibility when working with JPQL queries | [DATAJPA-292](https://jira.springsource.org/browse/DATAJPA-292) |


# Contributing

The [issue tracker](https://github.com/spring-projects/spring-petclinic/issues) is the preferred channel for bug reports, features requests and submitting pull requests.

For pull requests, editor preferences are available in the [editor config](.editorconfig) for easy use in common text editors. Read more and download plugins at <https://editorconfig.org>. If you have not previously done so, please fill out and submit the [Contributor License Agreement](https://cla.pivotal.io/sign/spring).

# License

The Spring PetClinic sample application is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).

[spring-petclinic]: https://github.com/spring-projects/spring-petclinic
[spring-framework-petclinic]: https://github.com/spring-petclinic/spring-framework-petclinic
[spring-petclinic-angularjs]: https://github.com/spring-petclinic/spring-petclinic-angularjs 
[javaconfig branch]: https://github.com/spring-petclinic/spring-framework-petclinic/tree/javaconfig
[spring-petclinic-angular]: https://github.com/spring-petclinic/spring-petclinic-angular
[spring-petclinic-microservices]: https://github.com/spring-petclinic/spring-petclinic-microservices
[spring-petclinic-reactjs]: https://github.com/spring-petclinic/spring-petclinic-reactjs
[spring-petclinic-graphql]: https://github.com/spring-petclinic/spring-petclinic-graphql
[spring-petclinic-kotlin]: https://github.com/spring-petclinic/spring-petclinic-kotlin
[spring-petclinic-rest]: https://github.com/spring-petclinic/spring-petclinic-rest
